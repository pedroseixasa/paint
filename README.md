# Paint

## Description
An application developed in Java that consists of a canvas with a simple paint and erase tool where we can make simple drawings, clean the canvas and save and load the drawing in the computer files.

## KeyBinds
- Arrows to move Cursor
- "Space" - Paint or Clear 
- "C" - Clear Screen
- "S" - Save
- "L" - Load
