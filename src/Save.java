import org.academiadecodigo.simplegraphics.graphics.Color;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Save {

    private int cols = 55; // Número de colunas
    private int rows = 55; // Número de linhas

    private int cellSize = 10;

    private Square[][] cells;

    private Square[][] cellstemp = new Square[cols][rows];

    public Save(){
    }

    public void createFile(Map map) throws IOException {
        // open an output stream with a file path as the destination
        FileOutputStream outputStream = new FileOutputStream("load.txt");

        this.cells = map.getCells();

        this.rows = map.getRows();

        this.cols = map.getCols();

        this.cellSize = map.getCellSize();

        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int x = col * cellSize + 10; // Posição x da célula
                int y = row * cellSize + 10; // Posição y da célula

                    if(cells[col][row].isPainted()){

                        // escreve 7 se tiver pintado
                        byte one = 0x37;
                        outputStream.write(one);
                    }else{

                        // escreve 6 se não tiver pintado
                        byte zero = 0x36;
                        outputStream.write(zero);
                    }
                }
        }

        // don't forget to close the output stream
        outputStream.close();

    }

     public Square[][] loadFile() throws IOException {

        // open an output stream with a file path as the destination
        // open an input stream with a file path as the source
        FileInputStream inputStream = new FileInputStream("load.txt");

         for (int col = 0; col < cols; col++) {
             for (int row = 0; row < rows; row++) {
                 int x = col * cellSize + 10; // Posição x da célula
                 int y = row * cellSize + 10; // Posição y da célula

                 cellstemp[col][row] = new Square(x, y, cellSize, cellSize); // criar quadrado
                 cellstemp[col][row].draw();

                 // read one byte from the file
                 int b = inputStream.read();
                 //System.out.println(b);
                 if (b == 0x37) {
                     cellstemp[col][row].setPainted(true);
                 }else{
                     cellstemp[col][row].setPainted(false);
                 }

             }
         }

        // don't forget to close the input stream
        inputStream.close();

        return cellstemp;
    }

}
