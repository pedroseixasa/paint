import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.io.FileNotFoundException;
import java.io.IOException;


public class Handler implements KeyboardHandler {
    private final Keyboard keyboard;
    private PaintCursor cursor;

    private Map map;

    private Save save;

    private Square[][] cells;

    public Handler(PaintCursor cursor, Map map){
        this.cursor = cursor;
        this.map = map;

        save = new Save();

        keyboard = new Keyboard(this);
        createKeyboardEvents();
    }

    public void createKeyboardEvents() {
        KeyboardEvent keyboardEventRight = new KeyboardEvent();
        keyboardEventRight.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboardEventRight.setKey(KeyboardEvent.KEY_RIGHT);
        keyboard.addEventListener(keyboardEventRight);

        KeyboardEvent keyboardEventLeft = new KeyboardEvent();
        keyboardEventLeft.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboardEventLeft.setKey(KeyboardEvent.KEY_LEFT);
        keyboard.addEventListener(keyboardEventLeft);

        KeyboardEvent keyboardEventSpace = new KeyboardEvent();
        keyboardEventSpace.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboardEventSpace.setKey(KeyboardEvent.KEY_SPACE);
        keyboard.addEventListener(keyboardEventSpace);

        KeyboardEvent keyboardEventUp = new KeyboardEvent();
        keyboardEventUp.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboardEventUp.setKey(KeyboardEvent.KEY_UP);
        keyboard.addEventListener(keyboardEventUp);

        KeyboardEvent keyboardEventDown = new KeyboardEvent();
        keyboardEventDown.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboardEventDown.setKey(KeyboardEvent.KEY_DOWN);
        keyboard.addEventListener(keyboardEventDown);

        KeyboardEvent keyboardEventClear = new KeyboardEvent();
        keyboardEventClear.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboardEventClear.setKey(KeyboardEvent.KEY_C);
        keyboard.addEventListener(keyboardEventClear);

        KeyboardEvent keyboardEventSave = new KeyboardEvent();
        keyboardEventSave.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboardEventSave.setKey(KeyboardEvent.KEY_S);
        keyboard.addEventListener(keyboardEventSave);

        KeyboardEvent keyboardEventLoad= new KeyboardEvent();
        keyboardEventLoad.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboardEventLoad.setKey(KeyboardEvent.KEY_L);
        keyboard.addEventListener(keyboardEventLoad);

    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_RIGHT:
                cursor.move(1);
                break;
            case KeyboardEvent.KEY_LEFT:
                cursor.move(2);
                break;

            case KeyboardEvent.KEY_DOWN:
                cursor.move(3);
                break;


            case KeyboardEvent.KEY_UP:
                cursor.move(4);
                break;

            case KeyboardEvent.KEY_SPACE:
                cursor.paint();
                break;

            case KeyboardEvent.KEY_C:
                cursor.clear(map);
                break;

            case KeyboardEvent.KEY_S:
                try {
                    save.createFile(map);
                } catch (FileNotFoundException e) {
                    throw new RuntimeException(e);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                break;

            case KeyboardEvent.KEY_L:
                cursor.clear(map);
                try {
                    this.cells = save.loadFile();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                map.loadMap(this.cells);

                break;

        }

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

}
