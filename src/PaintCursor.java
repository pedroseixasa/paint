import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class PaintCursor extends Square {

    private int width = 10; // largura
    private int height = 10; // altura

    private int cols;

    private int rows;

    private int cellSize;

    private Map map;

    private Square[][] cells;

    Square cursor = new Square(20, 20, this.width, this.height);
    public PaintCursor(Map map){

        cursor.fill();

        this.map = map;

        new Handler(this, map);

    }

    public void move(int direction){

        cells = map.getCells();

        // direita
        if(direction == 1 && (cursor.getX()) < map.getCols() * 10){
            cursor.translate(10, 0);
        }

        // esquerda
        if(direction == 2 && (cursor.getX() - 10) > 0){
            cursor.translate(-10, 0);
        }

        // cima
        if(direction == 4 && (cursor.getY() - 10) > 0){
            cursor.translate(0, -10);
        }

        // baixo
        if(direction == 3 && (cursor.getY()) < map.getRows() * 10){
            cursor.translate(0, 10);
        }
    }

    public void paint(){

        this.cells = map.getCells();

        this.rows = map.getRows();

        this.cols = map.getCols();

        this.cellSize = map.getCellSize();

        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int x = col * cellSize + 10; // Posição x da célula
                int y = row * cellSize + 10; // Posição y da célula

                if(cursor.getX() == x){
                    if(cursor.getY() == y){

                        if(cells[col][row].isPainted()){
                            cells[col][row].setColor(Color.WHITE);
                            cells[col][row].fill();
                            cells[col][row].setColor(Color.BLACK);
                            cells[col][row].draw();
                            cells[col][row].setPainted(false);
                        }else{
                            cells[col][row].setColor(Color.GREEN);
                            cells[col][row].fill();
                            cells[col][row].setPainted(true);
                        }

                    }
                }
            }
        }
    }

    public void clear(Map map){

        this.map = map;

        this.cells = this.map.getCells();

        this.rows = this.map.getRows();

        this.cols = this.map.getCols();

        this.cellSize = this.map.getCellSize();

        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int x = col * cellSize + 10; // Posição x da célula
                int y = row * cellSize + 10; // Posição y da célula

                if(cells[col][row].isPainted()){

                    cells[col][row].setColor(Color.WHITE);
                    cells[col][row].fill();
                    cells[col][row].setColor(Color.BLACK);
                    cells[col][row].draw();
                    cells[col][row].setPainted(false);
                }else{

                    cells[col][row].setColor(Color.BLACK);
                    cells[col][row].draw();
                    cells[col][row].setPainted(false);
                }

                }
            }

        }

    public void paintMap(Map map){


        this.map = map;

        this.cells = this.map.getCells();

        this.rows = this.map.getRows();

        this.cols = this.map.getCols();

        this.cellSize = this.map.getCellSize();

        map.deleteGrid();

        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int x = col * cellSize + 10; // Posição x da célula
                int y = row * cellSize + 10; // Posição y da célula

                if(cells[col][row].isPainted()){

                    cells[col][row].setColor(Color.GREEN);
                    cells[col][row].fill();
                    cells[col][row].setPainted(true);
                }else{

                    cells[col][row].setColor(Color.BLACK);
                    cells[col][row].draw();
                    cells[col][row].setPainted(false);
                }

            }
        }

    }
}



