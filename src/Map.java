import org.academiadecodigo.simplegraphics.graphics.Color;

public class Map {

    private int cols = 55; // Número de colunas
    private int rows = 55; // Número de linhas
    private int cellSize = 10; // Tamanho de cada célula

    private Square[][] cells;

    public Map() {

        cells = new Square[cols][rows];
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int x = col * cellSize + 10; // Posição x da célula
                int y = row * cellSize + 10; // Posição y da célula

                cells[col][row] = new Square(x, y, cellSize, cellSize); // criar quadrado

            }
        }

    }
    public void createGrid() {

        // Desenhar células
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int x = col * cellSize + 10; // Posição x da célula
                int y = row * cellSize + 10; // Posição y da célula

                cells[col][row].draw();
            }
        }

    }

    public void loadGrid(){

        // Desenhar células
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int x = col * cellSize + 10; // Posição x da célula
                int y = row * cellSize + 10; // Posição y da célula

                if(this.cells[col][row].isPainted()){

                    this.cells[col][row].setColor(Color.GREEN);
                    this.cells[col][row].fill();

                }else{
                    cells[col][row].setColor(Color.WHITE);
                    cells[col][row].fill();
                    cells[col][row].setColor(Color.BLACK);
                    cells[col][row].draw();
                    cells[col][row].setPainted(false);
                }
            }
        }

    }

    public void loadMap(Square[][] cells){
        this.cells=cells;

        loadGrid();
    }

    public void deleteGrid(){
        // Desenhar células
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int x = col * cellSize + 10; // Posição x da célula
                int y = row * cellSize + 10; // Posição y da célula

                cells[col][row].delete();
            }
        }
    }

    public Square[][] getCells(){
        return cells;
    }

    public int getCols(){
        return cols;
    }

    public int getRows(){
        return rows;
    }

    public int getCellSize(){
        return cellSize;
    }

    public Map getMap() {
        return this;
    }


}
